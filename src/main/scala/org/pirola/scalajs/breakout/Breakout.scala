package org.pirola.scalajs.breakout

import scala.language.implicitConversions
import scala.scalajs.js.annotation.JSExport
import org.scalajs.dom._
import org.scalajs.dom
import org.scalajs.dom.ext.KeyCode

import scala.collection.mutable.{ArrayBuffer => MutableArray}

@JSExport
object Breakout {
  private val ballRadius   = 10
  private val paddleHeight = 10
  private val paddleWidth  = 75

  /* Brick configuration */
  private val brickRowCount    = 3
  private val brickColumnCount = 5
  private val brickWidth       = 75
  private val brickHeight      = 20
  private val brickPadding     = 10
  private val brickOffsetTop   = 30
  private val brickOffsetLeft  = 30

  class Brick(val x: Int, val y: Int, var status: Int)

  private val bricks = createBricks

  @JSExport
  def start(canvas: html.Canvas) = {
    println("Start breakout class")

    val ctx: CanvasRenderingContext2D = canvas
      .getContext("2d")
      .asInstanceOf[dom.CanvasRenderingContext2D]

    var x            = canvas.width / 2
    var y            = canvas.height - 30
    var dx           = 2
    var dy           = -2
    var paddleX      = (canvas.width - paddleWidth) / 2
    var rightPressed = false
    var leftPressed  = false
    var score        = 0
    var lives        = 3

    def draw: Unit = {
      ctx.clearRect(0, 0, canvas.width, canvas.height)
      drawBricks
      drawBall
      drawPaddle
      drawScore
      drawLives
      collisionDetection
      if (x + dx > canvas.width - ballRadius || x + dx < ballRadius) dx = -dx

      if (y + dy < ballRadius) dy = -dy
      else if (y + dy > canvas.height - ballRadius) {
        if (x > paddleX && x < paddleX + paddleWidth) dy = -dy
        else {
          lives -= 1
          if (lives == 0) {
            dom.window.alert("GAME OVER")
            document.location.reload()
          } else {
            x = canvas.width / 2
            y = canvas.height - 30
            dx = 2
            dy = -2
            paddleX = (canvas.width - paddleWidth) / 2
          }
        }
      }

      x += dx
      y += dy
      // Paddle position
      if (rightPressed && paddleX < canvas.width - paddleWidth) paddleX += 7
      else if (leftPressed && paddleX > 0) paddleX -= 7
      window.requestAnimationFrame((d: Double) => draw)
    }

    def drawBall = {
      ctx.beginPath()
      ctx.arc(x, y, ballRadius, 0, Math.PI * 2)
      ctx.fillStyle = "#0095DD"
      ctx.fill()
      ctx.closePath()
    }

    def drawPaddle = {
      ctx.beginPath()
      ctx.rect(paddleX, canvas.height - paddleHeight, paddleWidth, paddleHeight)
      ctx.fillStyle = "#0095DD"
      ctx.fill()
      ctx.closePath()
    }

    def drawBricks =
      bricks.foreach(_.filter(_.status == 1).foreach { brick =>
        ctx.beginPath()
        ctx.rect(brick.x, brick.y, brickWidth, brickHeight)
        ctx.fillStyle = "#0095DD"
        ctx.fill()
        ctx.closePath()
      })

    def drawScore = {
      ctx.font = "16px Arial"
      ctx.fillStyle = "#0095DD"
      ctx.fillText("Score: " + score, 8, 20)
    }

    def drawLives = {
      ctx.font = "16px Arial"
      ctx.fillStyle = "#0095DD"
      ctx.fillText("Lives: " + lives, canvas.width - 65, 20)
    }

    document.addEventListener("keydown", keyDownHandler, false)
    document.addEventListener("keyup", keyUpHandler, false)
    canvas.onmousemove = (e: dom.MouseEvent) => mouseMoveHandler(e)

    def keyDownHandler =
      (e: dom.KeyboardEvent) =>
        if (e.keyCode == KeyCode.Right) rightPressed = true
        else if (e.keyCode == KeyCode.Left) leftPressed = true

    def keyUpHandler =
      (e: dom.KeyboardEvent) =>
        if (e.keyCode == KeyCode.Right) rightPressed = false
        else if (e.keyCode == KeyCode.Left) leftPressed = false

    def collisionDetection =
      bricks.foreach(_.filter(_.status == 1).foreach { brick =>
        if (x > brick.x && x < brick.x + brickWidth && y > brick.y && y < brick.y + brickHeight) {
          dy = -dy
          brick.status = 0
          score += 1
          if (score == brickRowCount * brickColumnCount) {
            dom.window.alert("YOU WIN, CONGRATULATIONS!")
            document.location.reload()
          }
        }
      })

    def mouseMoveHandler(e: dom.MouseEvent) = {
      val relativeX = (e.clientX - canvas.offsetLeft).toInt
      if (relativeX > 0 && relativeX < canvas.width)
        paddleX = relativeX - (paddleWidth / 2)
    }

    draw
  }

  private[this] def createBricks: MutableArray[MutableArray[Brick]] =
    MutableArray.range(0, brickColumnCount).map { x =>
      MutableArray.range(0, brickRowCount).map { y =>
        val brickX = (x * (brickWidth + brickPadding)) + brickOffsetLeft
        val brickY = (y * (brickHeight + brickPadding)) + brickOffsetTop
        new Brick(brickX, brickY, 1)
      }
    }
}
