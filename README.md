# scala-js-breakout
Implementation of Move The Ball tutorial available in Mozilla tutorials section (see https://developer.mozilla.org/en-US/docs/Games/Tutorials).

## Build
* `sbt ~fastOptJS` - for development
* `sbt fullOptJS` - for production
* `target/scala-2.11/classes/` - folder for output HTML files

## License
GNU Version 3