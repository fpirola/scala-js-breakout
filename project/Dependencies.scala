import sbt._
import org.scalajs.sbtplugin.ScalaJSPlugin.autoImport._

object Dependencies {
  val scalatest = "org.scalatest" %% "scalatest" % "2.2.4"

  lazy val library = Seq(
    scalatest % Test
  )

  val resolvers = Seq(
    Resolver.sonatypeRepo("public")
  )
}
